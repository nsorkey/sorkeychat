package edu.lsus.chaticus

import android.content.Context
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.*
import android.widget.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import okhttp3.RequestBody
import org.json.JSONObject


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        populator()

        val ET: EditText = findViewById(R.id.editMessage)
        val button: Button = findViewById(R.id.sendMessage)
        val actionBar = getActionBar()

        button.setOnClickListener {
            val messageString: String = "{ \"title\": \"" + ET.getText().toString() + "\" }"
            val MJSON = JSONObject(messageString)
            val body: RequestBody = RequestBody
                    .create(okhttp3.MediaType.parse("application/json; charset=utf-8"), MJSON.toString())
            app.api.postConversation(body).enqueue(object : Callback<MessageDto> {
                override fun onResponse(call: Call<MessageDto>?, response: Response<MessageDto>?) {
                    populator()
                    ET.setText("")
                }

                override fun onFailure(call: Call<MessageDto>?, t: Throwable?) {

                }
            })
        }
    }



    override fun onCreateOptionsMenu(menu: Menu?): Boolean
    {
        menuInflater.inflate(R.menu.main, menu)
        return super.onCreateOptionsMenu(menu)

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean = when (item.itemId) {
        R.id.refresh ->
        {
            45
            populator()
            true
        }
        else -> super.onOptionsItemSelected(item)
    }



    private class CustomAdapter(context: Context, rows: Int, conversationArray: Array<String>): BaseAdapter()
    {
        private val mContext: Context
        private val mRows: Int
        private val mConvoArray: Array<String>
        init
        {
            mContext = context
            mRows = rows
            mConvoArray = conversationArray
        }

        override fun getView(position: Int, convertView: View?, viewGroup: ViewGroup?): View
        {
            val layoutInflater = LayoutInflater.from(mContext)
            val convoRow = layoutInflater.inflate(R.layout.conversation_row, viewGroup, false)
            val idTextView = convoRow.findViewById<TextView>(R.id.convoId)
            val titleTextView = convoRow.findViewById<TextView>(R.id.convoTitle)
            val id: Int = position + 1
            idTextView.text = "Conversation ID: " + id.toString()
            titleTextView.text = "\"" + mConvoArray[position] + "\""

            return convoRow
        }

        override fun getItem(position: Int): Any {
            return "TEST"
        }

        override fun getCount(): Int {
            return mRows
        }

        override fun getItemId(position: Int): Long {
            return position.toLong()
        }
    }

    fun populator()
    {
        val messageList: ListView = findViewById<ListView>(R.id.list_of_messages)

        if (!app.session.hasValidCredentials())
        {
            app.login(this)
        }
        else {
            app.api.getMessages().enqueue(object : Callback<List<MessageDto>>
            {
                override fun onResponse(call: Call<List<MessageDto>>?, response: Response<List<MessageDto>>?)
                {
                    val messages : List<MessageDto>? = response?.body()
                    val size: Int = messages!!.size
                    val messagesArray = Array(size, { i -> messages!![i].title })
                    messageList.adapter = CustomAdapter(this@MainActivity, size, messagesArray)
                    messageList.setSelection(size)


                }

                override fun onFailure(call: Call<List<MessageDto>>?, t: Throwable?)
                {
                }
            })
        }
    }
}