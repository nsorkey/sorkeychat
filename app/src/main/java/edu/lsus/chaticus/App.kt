package edu.lsus.chaticus

import android.app.Activity
import android.app.Application
import android.app.Dialog
import android.content.Context
import com.auth0.android.Auth0
import com.auth0.android.authentication.AuthenticationAPIClient
import com.auth0.android.authentication.AuthenticationException
import com.auth0.android.authentication.storage.CredentialsManager
import com.auth0.android.authentication.storage.SharedPreferencesStorage
import com.auth0.android.provider.AuthCallback
import com.auth0.android.provider.WebAuthProvider
import com.auth0.android.result.Credentials
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

/**
 * Created by dculpepper on 4/23/18.
 */
class App : Application() {

    lateinit var auth0: Auth0
    lateinit var authClient: AuthenticationAPIClient
    lateinit var session: CredentialsManager
    lateinit var api: ChaticusApi

    override fun onCreate() {
        super.onCreate()
        auth0 = Auth0(BuildConfig.AUTH_0_CLIENT_ID, BuildConfig.AUTH_0_DOMAIN)
        authClient = AuthenticationAPIClient(auth0)
        session = CredentialsManager(authClient, SharedPreferencesStorage(this))
        api = Retrofit.Builder()
                .baseUrl("https://chaticus.herokuapp.com")
                .addConverterFactory(GsonConverterFactory.create())
                .client(OkHttpClient.Builder()
                        .addInterceptor(ApiAuthenticationInterceptor(session))
                        .build())
                .build()
                .create(ChaticusApi::class.java)

    }

    fun login(activity: Activity) {
        WebAuthProvider.init(Auth0(BuildConfig.AUTH_0_CLIENT_ID, BuildConfig.AUTH_0_DOMAIN))
                .withScheme("chaticus")
                .withScope("offline_access")
                .withAudience("https://${BuildConfig.AUTH_0_DOMAIN}/userinfo")
                .start(activity, object : AuthCallback {
                    override fun onFailure(dialog: Dialog) {
                        dialog.show()
                    }

                    override fun onFailure(exception: AuthenticationException) {
                    }

                    override fun onSuccess(credentials: Credentials) {
                        session.saveCredentials(credentials)
                    }
                })
    }

}

val Context.app: App
    get() = this.applicationContext as App